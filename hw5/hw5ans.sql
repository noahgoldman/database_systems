drop function recommend(varchar, integer) ;
create function recommend(input_category varchar, topusercount integer) returns varchar
AS $$
DECLARE
    favorite record;
    userRow record;
    rec record;
    businessRow record;
    A numeric;
    B numeric;
    outRow record;
    outText text;
BEGIN
    CREATE TABLE recommenders (
        seed_user varchar(40),
        similar_user varchar(40),
        vote int check (vote = 1 or vote = -1),
        similarity numeric,
        PRIMARY KEY (seed_user, similar_user)
    );

    CREATE TABLE scores (
        business_id varchar(40) primary key,
        name varchar(100),
        city varchar(255),
        score numeric
    );

    FOR favorite IN SELECT * FROM favorites
    LOOP
        -- Put this user into the recommenders
        INSERT INTO recommenders
        VALUES (favorite.user_id, favorite.user_id, favorite.vote, 1);
        -- Get all similar users
        FOR userRow IN
            SELECT
                favorite.user_id as u1,
                users.user_id as u2,
                favorite.vote as vote,
                (1 - avg(abs(r1.stars - r2.stars))/5) as similarity
            FROM
                users JOIN reviews as r1 ON r1.user_id = users.user_id,
                reviews as r2
            WHERE
                r2.user_id = favorite.user_id and
                r2.user_id != r1.user_id and
                r1.business_id = r2.business_id
            GROUP BY
                users.user_id
            HAVING
                count(*) >= 3
            ORDER BY
                similarity DESC,
                users.user_id ASC
            LIMIT topusercount
        LOOP
            -- Check if the similar_user is related to another seed_user with
            -- a vote opposite of this one
            IF EXISTS (
                    SELECT * FROM recommenders
                    WHERE
                        similar_user = userRow.u2::text and
                        seed_user = userRow.u1::text and
                        vote != userRow.vote) THEN
                DELETE FROM recommenders
                WHERE
                    similar_user = userRow.u2 and
                    seed_user = userRow.u1 and
                    vote != userRow.vote;
            -- Check if a similar_user is also favorite user, and if the vote
            -- is different.  If not, insert it into recommenders
            ELSIF NOT EXISTS (
                    SELECT *
                    FROM favorites
                    WHERE
                        favorites.user_id = userRow.u2 and
                        favorites.vote != userRow.vote) THEN
                -- Insert the row into the recommenders table
                INSERT INTO recommenders
                VALUES (userRow.u1, userRow.u2, userRow.vote, userRow.similarity);
            END IF;
        END LOOP;
    END LOOP;

    FOR businessRow IN
        SELECT DISTINCT
            businesses.business_id,
            businesses.name,
            businesses.city
        FROM
            businesses JOIN business_categories as bc ON businesses.business_id = bc.business_id
        WHERE
            bc.category IN (
                SELECT bc2.category
                FROM business_categories as bc1, business_categories as bc2
                WHERE
                    bc1.business_id = bc2.business_id and
                    bc1.category = input_category
                GROUP BY
                    bc2.category
                HAVING
                    count(*) >= 5)
    LOOP
        -- Calculate A
        A = 0;
        SELECT sum(recommenders.similarity * reviews.stars) INTO A
        FROM
            recommenders JOIN reviews ON recommenders.similar_user = reviews.user_id
        WHERE
            recommenders.vote = 1 and
            reviews.business_id = businessRow.business_id;
        IF A IS NULL THEN
            A = 0;
        END IF;

        -- Calculate B
        SELECT (0.1*sum(recommenders.similarity * reviews.stars)) INTO B
        FROM
            recommenders JOIN reviews ON recommenders.similar_user = reviews.user_id
        WHERE
            recommenders.vote = -1 and
            reviews.stars >= 4 and
            reviews.business_id = businessRow.business_id;
        IF B IS NULL THEN
            B = 0;
        END IF;

        INSERT INTO scores
        VALUES (businessRow.business_id, businessRow.name, businessRow.city,
                round(A - B, 2));
    END LOOP;

    outText = '';
    FOR outRow IN
        SELECT * FROM scores
        ORDER BY score DESC, name
        LIMIT 10
    LOOP
        outText = outText || rpad(outRow.name, 35) || rpad(outRow.city, 12) ||
            outRow.score || E'\n';
    END LOOP;

    -- Delete temp tables
    DROP TABLE IF EXISTS recommenders;
    DROP TABLE IF EXISTS scores;

    RETURN outText;
END ;
$$ LANGUAGE plpgsql ;
