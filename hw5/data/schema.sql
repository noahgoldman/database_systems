
drop table reviews ;
drop table users ;
drop table business_categories ;
drop table businesses ;

create table businesses (
    business_id    varchar(40) primary key
    , name         varchar(100)
    , full_address varchar(255)
    , isopen       boolean
    , photo_url    varchar(255)
    , city         varchar(255)
    , state        varchar(100)
    , review_count int
    , stars        numeric(4,2)
    , url          varchar(255)
    , latitude     float
    , longitude    float
) ;

create table business_categories (
    business_id    varchar(40)
    , category     varchar(40)
    , primary key (business_id, category)
) ;

create table users (
    user_id         varchar(40) primary key
    , funny_votes   int
    , useful_votes  int
    , cool_votes    int
    , name          varchar(100)
    , url           varchar(255)
    , average_stars numeric(4,2)
    , review_count  int
) ;

create table reviews (
    review_id       varchar(40) primary key
    , funny_votes   int
    , useful_votes  int
    , cool_votes    int
    , user_id       varchar(40)
    , stars         int
    , review_date   date
    , review_text   text
    , business_id   varchar(40)
    , constraint reviews_fk foreign key (business_id)
      references businesses(business_id)
    , constraint reviews_fk2 foreign key (user_id)
      references users(user_id)
) ;

drop function geodistance(float, float, float, float);

create function geodistance(latitude1 float, longitude1 float, latitude2 float, longitude2 float) returns float as $$
DECLARE
   lat1 float;
   long1 float;
   lat2 float;
   long2 float;
   a float ;
BEGIN
    lat1 = latitude1 * pi() / 180.0;
    long1 = longitude1 * pi() / 180.0;
    lat2 = latitude2 * pi() / 180.0;
    long2 = longitude2 * pi() / 180.0;
    a = sin((lat1-lat2)/2)^2 + cos(lat1) * cos(lat2) * sin((long1-long2)/2)^2;
    a = 2*tan( sqrt(a)/sqrt(1-a) );
    RETURN (6371 / 1.609)*a ;
END;
$$ LANGUAGE plpgsql;
