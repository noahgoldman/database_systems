\documentclass[11pt]{article}

\usepackage{amsmath}
\usepackage{geometry}
\usepackage{tikz-er2}

\renewcommand{\thesubsection}{\thesection.\alph{subsection}}

\usetikzlibrary{positioning}
\usetikzlibrary{shadows}

\tikzset{>=triangle 90}

\tikzstyle{every entity} = [top color=white, bottom color=blue!30, draw=blue!50!black!100, drop shadow]
\tikzstyle{every weak entity} = [drop shadow={shadow xshift=.7ex, shadow yshift=-.7ex}]
\tikzstyle{every attribute} = [top color=white, bottom color=yellow!20, draw=yellow, node distance=1cm, drop shadow]
\tikzstyle{every relationship} = [top color=white, bottom color=red!20, draw=red!50!black!100, drop shadow]
\tikzstyle{every isa} = [top color=white, bottom color=green!20, draw=green!50!black!100, drop shadow]


\geometry{a4paper,margin=1in}

\begin{document}

\title{Homework 2}
\author{Noah Goldman}
\maketitle

\section{For the following relations: first find the keys, then discuss whether it is in 3NF and/or BCNF with a short explanation of why or why not.}

\subsection{$R(A, B, C, D, E, F), \mathcal{F} = \{ABC \rightarrow DEF, CE \rightarrow A\}$}

The key of the relation can be found by examining $\mathcal{F}$.
\newline

\noindent
\textbf{Keys:} $\{\{A, B, C\}, \{B,C,E\}\}$
\newline

Since the left side of the second functional dependency $CE \rightarrow A$ is not a superkey for either of the possible keys, the relation is not in BCNF.  The relation is also not in 3NF as both $D$ and $F$ on the right hand side of $ABC \rightarrow DEF$ are not prime attributes.

\subsection{$R(A, B, C, D, E, F), \mathcal{F} = \{AB \rightarrow CDE, ABE \rightarrow F\}$}

\noindent
\textbf{Keys:} $\{\{A, B\}\}$
\newline

This relation is is BCNF, as both functional dependencies contain a superkey on their left hand sides.  As all relations in BCNF are also in 3NF, This relation is in 3NF as well.

\subsection{$R(A, B, C, D, E, F), \mathcal{F} = \{AB \rightarrow DE, CD \rightarrow A\}$}

\noindent
\textbf{Keys:} $\{\{A,B,C,F\}, \{B,C,D,F\}\}$
\newline

This relation is not in BCNF or 3NF.  The left side of both functional dependencies are not superkeys for either of the given keys to the relation.  Additionally, the $E$ on the right side of $AB \rightarrow DE$ isn't a prime attribute.  This shows that R isn't in BCNF or 3NF.

\subsection{$R(A, B, C, D, E, F), \mathcal{F} = \{BCD \rightarrow AEF, AB \rightarrow C\}$}

\noindent
\textbf{Keys:} $\{\{A,B,D\}, \{B,C,D\}\}$
\newline

This relation is also not in BCNF or 3NF.  The left side of both functional dependencies are not superkeys for either of the given keys to the relation.  Additionally, the $E$ and $F$ on the right hand side of $BCD \rightarrow AEF$ are both not prime attributes.  This shows that R isn't in BCNF or 3NF.

\section{You are given the following set $\mathcal{F}$ of functional dependencies. Convert this to a minimal basis. Show your work at each step by providing sufficient detail. You do not have to show all unsuccessful steps (what you tried to remove and decided cannot be removed), but you must show what needs to be removed and why. \\ \\ $R(A, B, C, D, E, F, G)$ with $\mathcal{F} = \{BC \rightarrow ADE, ABD \rightarrow BEF, E \rightarrow A, ABC \rightarrow G\}$}

The first step of finding a minimal basis of $\mathcal{F}$ is to attempt to remove any functional dependency from $\mathcal{F}$ such that the closure of the left hand side of the functional dependency doesn't change between the original set $\mathcal{F}$ and the set with the functional dependency removed.  For this particular set $\mathcal{F}$, there is no functional dependency that would be able to satisfy these conditions.  All except the functional dependency $ABC \rightarrow G$ would produce a closure containing simply the left hand side attributes on the altered set $\mathcal{F}$, and even removing $ABC \rightarrow G$ would cause the closure computed with the altered set to lack $G$, which the closure from the original set $F$ would contain.

The second step is to remove unnecessary attributes from the left hand side of functional dependencies.  This can be done in one case, where $ABC \rightarrow G$ can be replaced with $BC \rightarrow G$.  The closure of $\{B,C\}$ on the original set $\mathcal{F}$ is $\{B,C\}^+ = \{A,B,C,D,E,F,G\}$, and the closure on the altered set $\mathcal{F}=\{BC \rightarrow ADE, ABD \rightarrow BEF, E \rightarrow A, BC \rightarrow G\}$ is $\{B,C\}^+= \{A,B,C,D,E,F,G\}$.  Additionally, $ABD \rightarrow BEF$ can be replaced by $ABD \rightarrow EF$ as the addition of $B$ on the right hand side is trivial.

Finally, the combining rule for functional dependencies can be used to concatenate $BC \rightarrow ADE$ and $BC \rightarrow G$ into a single functional dependency.  This produces the final minimal basis $\mathcal{F}_{min} = \{BC \rightarrow ADEG, ABD \rightarrow EF, E \rightarrow A\}$.

\section{You are given the following relation: $R(A,B,C,D,E,F), \mathcal{F} = \{A \rightarrow C, CE \rightarrow D, CD \rightarrow A, DF \rightarrow A\}$.  Find whether the following decompositions of this relation are lossless or not using the Chase algorithm.}

\subsection{Decomposition 1: $R1(A, C, E, D)$, $R2(B, E, F)$, $R3(A, E, F)$}

First, represent the decomposition as a relation of tuples according to the Chase algorithm.

\begin{center}
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    \textbf{A} & \textbf{B} & \textbf{C} & \textbf{D} & \textbf{E} & \textbf{F} \\ \hline
    $a$ & $b_1$ & $c$ & $d$ & $e$ & $f_1$ \\ \hline
    $a_2$ & $b$ & $c_2$ & $d_2$ & $e$ & $f$ \\ \hline
    $a$ & $b_3$ & $c_3$ & $d_3$ & $e$ & $f$ \\ \hline
  \end{tabular}
\end{center}

\noindent
Apply $A \rightarrow C$ to $R1$ and $R3$.

\begin{center}
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    \textbf{A} & \textbf{B} & \textbf{C} & \textbf{D} & \textbf{E} & \textbf{F} \\ \hline
    $a$ & $b_1$ & $c$ & $d$ & $e$ & $f_1$ \\ \hline
    $a_2$ & $b$ & $c_2$ & $d_2$ & $e$ & $f$ \\ \hline
    $a$ & $b_3$ & $c$ & $d_3$ & $e$ & $f$ \\ \hline
  \end{tabular}
\end{center}

\noindent
Apply $CE \rightarrow D$ to $R1$ and $R3$.

\begin{center}
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    \textbf{A} & \textbf{B} & \textbf{C} & \textbf{D} & \textbf{E} & \textbf{F} \\ \hline
    $a$ & $b_1$ & $c$ & $d$ & $e$ & $f_1$ \\ \hline
    $a_2$ & $b$ & $c_2$ & $d_2$ & $e$ & $f$ \\ \hline
    $a$ & $b_3$ & $c$ & $d$ & $e$ & $f$ \\ \hline
  \end{tabular}
\end{center}

No other functional dependencies can be applied, and none of the tuples above have been reduced to have no subscripts.  Therefore, this given decomposition is lossy (not lossless).

\subsection{Decomposition 2: $R1(A,B,C,D), R2(A,D,E), R3(B,C,E,F)$}

Represent the decomposition as a relation of tuples according to the Chase algorithm.

\begin{center}
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    \textbf{A} & \textbf{B} & \textbf{C} & \textbf{D} & \textbf{E} & \textbf{F} \\ \hline
    $a$ & $b$ & $c$ & $d$ & $e_1$ & $f_1$ \\ \hline
    $a$ & $b_2$ & $c_2$ & $d$ & $e$ & $f_2$ \\ \hline
    $a_3$ & $b$ & $c$ & $d_3$ & $e$ & $f$ \\ \hline
  \end{tabular}
\end{center}

\noindent
Apply $A \rightarrow C$ to $R1$ and $R2$.

\begin{center}
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    \textbf{A} & \textbf{B} & \textbf{C} & \textbf{D} & \textbf{E} & \textbf{F} \\ \hline
    $a$ & $b$ & $c$ & $d$ & $e_1$ & $f_1$ \\ \hline
    $a$ & $b_2$ & $c$ & $d$ & $e$ & $f_2$ \\ \hline
    $a_3$ & $b$ & $c$ & $d_3$ & $e$ & $f$ \\ \hline
  \end{tabular}
\end{center}

\noindent
Apply $CE \rightarrow D$ to $R2$ and $R3$.

\begin{center}
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    \textbf{A} & \textbf{B} & \textbf{C} & \textbf{D} & \textbf{E} & \textbf{F} \\ \hline
    $a$ & $b$ & $c$ & $d$ & $e_1$ & $f_1$ \\ \hline
    $a$ & $b_2$ & $c$ & $d$ & $e$ & $f_2$ \\ \hline
    $a_3$ & $b$ & $c$ & $d$ & $e$ & $f$ \\ \hline
  \end{tabular}
\end{center}

\noindent
Apply $CD \rightarrow A$ to $R2$ and $R3$.

\begin{center}
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    \textbf{A} & \textbf{B} & \textbf{C} & \textbf{D} & \textbf{E} & \textbf{F} \\ \hline
    $a$ & $b$ & $c$ & $d$ & $e_1$ & $f_1$ \\ \hline
    $a$ & $b_2$ & $c$ & $d$ & $e$ & $f_2$ \\ \hline
    $a$ & $b$ & $c$ & $d$ & $e$ & $f$ \\ \hline
  \end{tabular}
\end{center}

\noindent
Since the last relation contains no subscripts, the decomposition is lossless.

\section{You are given the following relation: $R(A,B,C,D,E,F),\mathcal{F} = \{A \rightarrow BC, C \rightarrow D, D \rightarrow E\}$.  Convert it to BCNF using the BCNF decomposition, starting with the functional dependency: $C \rightarrow D$.}

Inspection shows that the given functional dependencies $\mathcal{F}$ are already a minimal basis.  No attributes can be removed from the left sides of the functional dependencies (as each has only one attribute on the left side), and no functional dependency can be removed as the closure of the left-hand side attribute would clearly not be equivalent.  Therefore, $\mathcal{F}$ is a minimal basis.

As neither $A$ nor $F$ appear on the right hand side of any functional dependency in $\mathcal{F}$.  As the closure of the two attributes $\{A,F\}^+ = {A,B,C,D,E,F}$ contains all attributes in R, the key for the relation must be $\{A,F\}$.

Using the functional dependency $C \rightarrow D$ as given in the problem statement decomposes $R$ into $R_1(C,D,E)$ and $R_2(A,B,C,F)$.  Projecting the functional dependencies onto both relations produces the sets $\mathcal{F}_1 = \{C \rightarrow D, D \rightarrow E\}$ and $\mathcal{F}_2 = \{A \rightarrow BC\}$.

\begin{align*}
  &R_1(C,D,E),\mathcal{F}_1 = \{C \rightarrow D, D, \rightarrow E\} \\
  &R_2(A,B,C,F),\mathcal{F}_2 = \{A \rightarrow BC\}
\end{align*}

The key of relation $R_1$ is clearly $C$ by inspection, but the functional dependency $D \rightarrow E$ violates BCNF.  $R_1$ can then be decomposed using $D \rightarrow E$ into $R_3(D,E)$ and $R_4(C,D)$.  The functional dependencies can again be projected, and the result is shown below.

\begin{align*}
  &R_2(A,B,C,F),\mathcal{F}_2 = \{A \rightarrow BC\} \\
  &R_3(D,E),\mathcal{F}_3 = \{D \rightarrow E\} \\
  &R_4(C,D),\mathcal{F}_4 = \{C \rightarrow D\}
\end{align*}

Both $R_3$ and $R_4$ are now in BCNF, however $R_2$ is not.  It can be further decomposed with $A \rightarrow BC$ to create $R_5(A,B,C)$ and $R_6(A,F)$.  Again, functional dependencies are projected onto these relations.

\begin{align*}
  &R_3(D,E),\mathcal{F}_3 = \{D \rightarrow E\} \\
  &R_4(C,D),\mathcal{F}_4 = \{C \rightarrow D\} \\
  &R_5(A,B,C),\mathcal{F}_5 = \{A \rightarrow BC\} \\
  &R_6(A,F),\mathcal{F}_6 = \{\} \\
\end{align*}

The resulting relations above ($R_3,R_4,R_5,R_6$) are all in BCNF and thus are a valid decomposition.  Taking the union of all sets of functional dependencies produces $\mathcal{F}_3 \cup \mathcal{F}_4 \cup \mathcal{F}_5 \cup \mathcal{F}_6 = \{A \rightarrow BC, C \rightarrow D, D \rightarrow E\}$, which is the same as $\mathcal{F}$.  Therefore, this decomposition is dependency preserving.

\section{Convert the relation from Question 4 to 3NF using the 3NF decomposition. Show your work.}

As shown in Question 4, the set of functional dependencies $\mathcal{F}$ is already a minimal basis.  The decomposition set can then be initialized to $Decomp = \{\}$.

The dependency $A \rightarrow BC$ can be used to add a relation $R_1(A,B,C)$ to $Decomp$, as no relation exists in $Decomp$ already containing $A,B,C$.  The same logic holds for the functional dependency $C \rightarrow D$, producing a relation $R_2(C,D)$.  Finally the dependency $D \rightarrow E$ will create a relation $R_3(D,E)$.

\[Decomp = \{R_1(A,B,C),R_2(C,D),R_3(D,E)\}\]

The solution to Question 4 gives the key of $R(A,B,C,D,E,F)$ as $\{A,F\}$.  Since no relation in $Decomp$ contains both $A$ and $F$, a new relation $R_4(A,F)$ will be added to the set.  This produces the relations shown below.

\begin{align*}
  &R_1(A,B,C), \mathcal{F}_1 = \{A \rightarrow BC\} \\
  &R_2(C,D), \mathcal{F}_2 = \{C \rightarrow D\} \\
  &R_3(D,E), \mathcal{F}_3 = \{D \rightarrow E\} \\
  &R_4(A,F), \mathcal{F}_4 = \{\}
\end{align*}

The relations shown above are a valid, dependency-preserving 3NF decomposition of $R$.

\section{ER Diagram}

Several assumptions were made when constructing the ER diagram.  The type of election (presidential, state, senate, etc...) was assumed to be representable by a single value \textit{electionType} that appears in both the Primary and Election entities.  This is due to the fact that the homework prompt specifically says that other elections can exist without specifying what they are, so the attribute to store that value should be very flexible and be able to accept a wide range of values.  Additionally, several attributes that weren't specifically mentioned in the homework prompt were added to entities to serve as keys, reducing the number of entities that needed explicit \textit{id} attributes.  Lastly, the \textit{Standing} attribute for the \textit{Has position on} relationships represents a boolean value of the organization or candidate being either pro or con on the associated issue.

Finally, the diagramming software used was not able to generate the rounded arrow shown in class and the online notes.  The double arrow symbol is used instead.

\resizebox{\linewidth}{!}{
\begin{tikzpicture}[node distance=8em]
  \node[entity] (voter) {\begin{tabular} {c} \textbf{Voter} \\
    \key{id} \\
    firstName \\
    lastName \\
    middleInitial \\
    gender \\
    birthDate \\
    phoneNumber \\
    email \\
    haveVoted \\
    yearVoted \\
    employer \\
    occupation
  \end{tabular}};
  \node[relationship] (livesAt) [above left of=voter, node distance=12em] {Lives at} edge (voter);
  \node[relationship] (mailingAddress) [left of=voter, node distance=12em] {Mailing address} edge (voter);

  \node[ident relationship] (dmvRel) [above of=livesAt] {Has DMV num} edge [->>] (voter);
  \node[weak entity] (dmv) [above of=dmvRel] {\begin{tabular}{c} \textbf{DMVNumber} \\ \key{number} \end{tabular}} edge [total] (dmvRel);

  \node[ident relationship] (ssnRel) [above of=voter, node distance=13em] {Has SSN} edge [->>] (voter);
  \node[weak entity] (ssn) [above of=ssnRel] {\begin{tabular}{c} \textbf{SSN} \\ \key{number} \end{tabular}} edge [total] (ssnRel);

  \node[relationship] (votedIn) [right of=voter, node distance=12em] {Voted in} edge (voter);

  \node[entity] (election) [right of=votedIn] {\begin{tabular}{c} \textbf{Election} \\
    \key{year} \\
    \key{electionType} \\
  \end{tabular}} edge (votedIn);

  \node[entity] (primary) [above of=election, node distance=6em] {\begin{tabular}{c}
    \textbf{Primary} \\
    \key{year} \\
    \key{electionType}
  \end{tabular}};
  \node[relationship] (forParty) [right of=primary] {For party} edge (primary);
  \node[relationship] (votedInPrimary) [left of=primary] {Voted in} edge (primary) edge (voter);

  \node[relationship] (candidateParty) [below right of=election] {Belongs to} edge (election);
  \node[entity] (party) [below right of=candidateParty] {\begin{tabular}{c} \textbf{Party} \\
    \underline{name} \end{tabular}} edge [<-] (voter) edge [<<-] (forParty) edge [<-] (candidateParty);

  \node[relationship] (relOrganization) [below left of=voter, node distance=12em] {Donates to} edge (voter);
  \node[attribute] (orgDonorAmt) [left of=relOrganization, node distance=8em] {Amount} edge (relOrganization);
  \node[attribute] (orgDonorDate) [below left of=relOrganization, node distance=6em] {Date} edge (relOrganization);
  \node[entity] (charitableOrg) [below of=relOrganization] {
    \begin{tabular}{c} \textbf{CharitableOrganization} \\
      \key{name}
    \end{tabular}} edge (relOrganization);

  \node[relationship] (relCandidate) [below right of=voter, node distance=12em] {Donates to} edge (voter);
  \node[attribute] (cadidateDonorAmt) [right of=relCandidate, node distance=7em] {Amount} edge (relCandidate);
  \node[entity] (candidate) [below of=relCandidate] {\begin{tabular}{c}
    \textbf{Candidate} \\
    \key{id} \\
    fname \\
    lname
  \end{tabular}} edge (relCandidate) edge (candidateParty);

  \node[relationship] (relCandidateIssue) [right of=candidate, node distance=12em] {Has position on} edge (candidate) edge [->] (election);
  \node[attribute] (standing1) [right of=relCandidateIssue, node distance=10em] {Standing} edge (relCandidateIssue);
  \node[entity] (issue) [below of=relCandidateIssue] {\begin{tabular}{c}
    \textbf{Issue} \\
    \key{name} \\
    description
  \end{tabular}} edge (relCandidateIssue);
  \node[relationship] (relOrgIssue) [left of=issue, node distance=10em] {Has position on} edge (issue) edge (charitableOrg);
  \node[attribute] (standing1) [below of=relOrgIssue, node distance=8em] {Standing} edge (relOrgIssue);

  \node[relationship] (relSocialUser) [below of=voter, node distance=25em] {Has} edge [->] (voter);
  \node[entity] (socialMediaUser) [below of=relSocialUser] {\begin{tabular}{c}
    \textbf{SocialMediaUsername} \\
    \key{username}
  \end{tabular}} edge (relSocialUser);
  \node[relationship] (relOutlet) [above left of=socialMediaUser] {On website} edge [->] (socialMediaUser);
  \node[entity] (socialMediaOutlet) [left of=relOutlet, node distance=10em] {\begin{tabular}{c}
    \textbf{SocialMediaOutlet} \\
    name \\
    \key{URL}
  \end{tabular}} [<-] edge(relOutlet);
  \node[relationship] (relAnalysis) [below of=socialMediaUser] {Is analyzed} edge [->>] (socialMediaUser);
  \node[entity] (socialMediaAnalysis) [below of=relAnalysis] {\begin{tabular}{c}
    \textbf{SocialMediaAnalysis} \\
    \key{id} \\
    numFriends \\
    numMessage \\
    date
  \end{tabular}} edge (relAnalysis);

  \node[relationship] (relAnalIssues) [above right of=socialMediaAnalysis, node distance=12em] {Talks about}
    edge (socialMediaAnalysis) edge (issue);
  \node[attribute] (percent) [right of=relAnalIssues, node distance=13em] {Percent of messages about} edge (relAnalIssues);

  \node[entity] (address) [left of=livesAt] {\begin{tabular}{c}
    \textbf{Address} \\
    \key{state} \\
    \key{city} \\
    zip \\
    \key{street} \\
    \key{aptNumber}
  \end{tabular}} edge [<<-] (livesAt) edge [<<-] (mailingAddress);

  \node[relationship] (subMagazine) [above of=primary, node distance=10em] {Subscribes to} edge (voter);
  \node[relationship] (prevSub) [right of=ssnRel] {Subscribed to} edge (voter);
  \node[entity] (magazine) [above of=prevSub] {\begin{tabular}{c}
    \textbf{Magazine} \\
    name \\
    \key{URL}
  \end{tabular}} edge (prevSub) edge (subMagazine);
\end{tikzpicture}
}

\end{document}
