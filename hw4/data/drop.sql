
drop table movies;
drop table actors ;
drop table directors ;

drop table moviedirectors ;
drop table movieroles;
drop table moviegenres;
drop table movieplots ;
drop table imdbratings;

drop table twittergenres;
drop table twitterusers;
drop table twitterratings;
