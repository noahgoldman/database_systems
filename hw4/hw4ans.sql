-- Change this with your RCS first
SELECT 'Student: Noah Goldman (goldmn@rpi.edu)';

SELECT 'Question 1';

SELECT
    movies.id,
    movies.name,
    movies.movieyear,
    imdbratings.rating as irating,
    imdbratings.numvotes as ivotes,
    round(avg(tr.rating), 2) as trating,
    count(*) as tvotes
FROM
    imdbratings,
    movies INNER JOIN twitterratings as tr ON (movies.id = tr.movieid)
WHERE
    imdbratings.movieid = movies.id and
    movies.name LIKE '%Harry Potter%'
GROUP BY
    movies.id,
    movies.name,
    movies.movieyear,
    imdbratings.rating,
    imdbratings.numvotes
ORDER BY
    movies.movieyear DESC;


SELECT 'Question 2';

SELECT
    movies.id,
    movies.name,
    imdbratings.rating
FROM
    movies JOIN imdbratings ON (movies.id = imdbratings.movieid)
WHERE
    movies.id = (
        SELECT movieid FROM imdbratings as i1 WHERE i1.rating = (
            SELECT min(rating) FROM imdbratings
        )
    );


SELECT 'Question 3';

SELECT
    movies.id,
    movies.name,
    round(avg(tr.rating), 2) as avg
FROM
    movies JOIN twitterratings as tr on (movies.id = tr.movieid)
GROUP BY
    movies.id,
    movies.name
HAVING
    avg(tr.rating) = (
        SELECT min(trAvg.avg) FROM (
           SELECT avg(rating) from twitterratings as tr4 group by tr4.movieid)
    as trAvg);


SELECT 'Question 4';

SELECT DISTINCT
    actors.id,
    actors.name,
    actors.surname
FROM
    actors
        JOIN movieroles as mr1 ON actors.id = mr1.actorid
        JOIN movieroles as mr2 ON actors.id = mr2.actorid
        JOIN movies as m1 ON m1.id = mr1.movieid
        JOIN movies as m2 ON m2.id = mr2.movieid
        JOIN imdbratings as i1 on i1.movieid = m1.id
        JOIN imdbratings as i2 on i2.movieid = m2.id
WHERE
    i1.rating >  8 and
    i2.rating < 2
ORDER BY actors.id ASC;


SELECT 'Question 5';

SELECT
    movies.id,
    movies.name
FROM
    movies JOIN movieplots as mp ON mp.movieid = movies.id
WHERE
    mp.plot LIKE '%destruction%' and
    mp.plot LIKE '%Earth%' and
    movies.id NOT IN (
        SELECT mg.movieid FROM moviegenres as mg WHERE mg.genre = 'Sci-Fi');


SELECT 'Question 6';

SELECT
    top2.name
FROM
    (
        SELECT movies.id, movies.name, ir.rating
        FROM movies JOIN imdbratings as ir ON ir.movieid = movies.id
        WHERE movies.name LIKE '%Harry Potter%'
        GROUP BY movies.id, movies.name, ir.rating  ORDER BY ir.rating DESC LIMIT 2
    ) as top2
ORDER BY top2.rating ASC LIMIT 1;


SELECT 'Question 7';

SELECT
    tu.id,
    concat('https://twitter.com/intent/user?user_id=', tu.twitterid) as url
FROM
    twitterusers as tu JOIN twitterratings as tr ON tu.id = tr.userid
WHERE
    tr.rating < 3
GROUP BY tu.id
HAVING
    count(*) >= 10
EXCEPT
SELECT
    tu.id,
    concat('https://twitter.com/intent/user?user_id=', tu.twitterid) as url
FROM twitterusers as tu JOIN twitterratings as tr ON tu.id = tr.userid
WHERE tr.rating > 8
ORDER BY id;


SELECT 'Question 8';

SELECT
    movies.id,
    movies.name,
    ir.rating as imdbrating,
    round(tr.avgRating, 2) as twitterrating
FROM
    movies
        JOIN imdbratings as ir ON ir.movieid = movies.id
        JOIN (
            SELECT movieid, avg(twitterratings.rating) as avgRating
            FROM twitterratings
            GROUP BY twitterratings.movieid
            HAVING count(*) >= 5)
        as tr ON tr.movieid = movies.id
WHERE
    abs(ir.rating  - tr.avgRating) > 3
ORDER BY abs(ir.rating - tr.avgRating);


SELECT 'Question 9';

SELECT
    actors.id,
    actors.name,
    actors.surname,
    count(*) as nummovies,
    min(ir.rating) as minirating,
    max(ir.rating) as maxirating,
    round(avg(ir.rating)::numeric, 2) as avgirating,
    (
        SELECT avg(twitterratings.rating)
        FROM twitterratings JOIN movieroles ON twitterratings.movieid = movieroles.movieid
        WHERE movieroles.actorid = actors.id
    ) as avgtrating
FROM
    actors
        JOIN movieroles as mr ON actors.id = mr.actorid
        JOIN movies ON movies.id = mr.movieid
        JOIN imdbratings as ir ON ir.movieid = movies.id
GROUP BY
    actors.id,
    actors.name,
    actors.surname
HAVING
    count(*) >= 50;

SELECT 'Question 10';

SELECT
    directors.id,
    directors.name,
    directors.surname
FROM
    directors
        JOIN moviedirectors as md ON directors.id = md.directorid
        JOIN actors ON (actors.name = directors.name and actors.surname = directors.surname)
        LEFT JOIN movieroles as mr ON (mr.actorid = actors.id and mr.movieid = md.movieid)
WHERE
    directors.id IN (
        SELECT directors.id
        FROM directors JOIN moviedirectors ON directors.id = moviedirectors.directorid
        GROUP BY directors.id
        HAVING count(*) >= 5
    )
GROUP BY
    directors.id,
    directors.name,
    directors.surname
HAVING
    (count(mr.actorid) / count(*)) > 0.9
ORDER BY directors.id;
