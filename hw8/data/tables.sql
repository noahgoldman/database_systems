
-- IMDB data here
create table movies(
    id          serial primary key
    , title     text not null
    , full_name text
    , type      varchar(20)
    , ep_name   text
    , ep_num    text
    , status    text
    , years     text
    , movieyear int
    , name      varchar(150)
) ;
create index movies_idx on movies(title, id);

create table actors (
    id        serial primary key
    , type    varchar(10)  -- actor or actress 
    , name    varchar(127)
    , surname varchar(127)
) ;
create index actors_idx on actors(name, surname, type, id) ;

create table directors (
    id        serial primary key
    , name    varchar(127)
    , surname varchar(127)
) ;

create table moviedirectors (
    directorid int
    , movieid  int
    , info     text
    , primary key (directorid, movieid)
);    

create table movieroles (
   actorid     int
   , movieid   int
   , info_1    text
   , info_2    text
   , role      text
   , primary key (actorid, movieid)
) ;

create table moviegenres (
   movieid    int
   , genre    text
   , primary key (movieid, genre)
) ;    

create table movieplots(
   movieid    int primary key
   , plot     text
) ;

create table imdbratings(
   movieid    int primary key
   , distribution varchar(127) not null
   , rating   float
   , numvotes int
) ;    

-- Data mined from Twitter for
-- users rating movies

create table twittergenres (
   movieid int
   , genre varchar(100)
   , primary key (movieid, genre)
) ;

create table twitterusers (
   id int  primary key
   , twitterid bigint);

create table twitterratings (
   userid int
   , movieid int
   , rating int
   , whenrated timestamp
   , primary key (userid, movieid)
);

