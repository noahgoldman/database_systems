create index movieroles_idx on movieroles(actorid, movieid);
create index twitterratings_rating_idx on twitterratings(rating);
create index twitterratings_pk_rating_idx
    on twitterratings(userid, movieid, rating);
