import sys
import re
import os

data = sys.stdin.read()

r = re.compile(r'image\:\:\s(.+)/(.+\.png)')

for m in r.findall(data):
    if not os.path.exists(m[0]):
        os.mkdir(m[0])
    os.system("curl http://www.cs.rpi.edu/~sibel/csci4380/_images/{} > {}/{}".format(m[1], m[0], m[1]))
