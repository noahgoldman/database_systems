SELECT 'Student: Noah Goldman (goldmn@rpi.edu)';

SELECT 'Question 1';

SELECT
    users.name as name,
    businesses.name as name,
    reviews.review_date
FROM users, businesses, reviews
WHERE
    reviews.business_id = businesses.business_id and
    reviews.user_id = users.user_id and
    businesses.name = 'DeFazio''s Pizzeria' and
    reviews.stars = 5 and
    reviews.review_date >= '2012-01-01'
ORDER BY reviews.review_date;


SELECT 'Question 2';

SELECT DISTINCT bc1.category
FROM business_categories bc1, business_categories bc2, businesses
WHERE
    bc1.business_id = businesses.business_id and
    bc2.business_id = businesses.business_id and
    bc1.category != 'Active Life' and
    bc2.category = 'Active Life' and
    businesses.city = 'Troy'
ORDER BY bc1.category;


SELECT 'Question 3';

SELECT
    max(reviews.funny_votes) as maxfunny,
    max(reviews.useful_votes) as maxuseful,
    max(reviews.cool_votes) as maxcool
FROM reviews, businesses
WHERE
    reviews.business_id = businesses.business_id and
    businesses.city = 'Troy';


SELECT 'Question 4';

SELECT
    businesses.name,
    reviews.review_date,
    reviews.review_text
FROM reviews, businesses
WHERE
    reviews.business_id = businesses.business_id and
    reviews.funny_votes = 17;


SELECT 'Question 5';

SELECT users.user_id, users.name FROM users
EXCEPT
SELECT users.user_id, users.name FROM reviews, users
WHERE reviews.user_id = users.user_id;


SELECT 'Question 6';

SELECT
    users.user_id,
    users.name
FROM reviews, users
WHERE
    reviews.user_id = users.user_id and
    users.review_count >= 100
GROUP BY users.user_id, users.name
HAVING count(*) >= 20;


SELECT 'Question 7';

SELECT
    businesses.business_id,
    businesses.name,
    businesses.stars,
    businesses.review_count,
    businesses.full_address
FROM businesses, reviews, business_categories as bc
WHERE
    reviews.business_id = businesses.business_id and
    businesses.review_count >= 10 and
    businesses.stars >= 4 and
    bc.business_id = businesses.business_id and
    bc.category = 'Restaurants' and
    businesses.isopen = TRUE
EXCEPT
SELECT
    businesses.business_id as id,
    businesses.name,
    businesses.stars as average_stars,
    businesses.review_count,
    businesses.full_address
FROM businesses, reviews, business_categories as bc
WHERE
    reviews.business_id = businesses.business_id and
    reviews.review_date >= '2009-01-01' and
    reviews.stars <= 2 and
    (reviews.funny_votes > 0 or reviews.useful_votes > 0 or
        reviews.cool_votes > 0) and
    bc.business_id = businesses.business_id and
    bc.category = 'Restaurants'
ORDER BY name;

SELECT 'Question 8';

SELECT
    b.name,
    round(geodistance(rpi.latitude, rpi.longitude, b.latitude, b.longitude)::numeric, 2) as dist
FROM businesses b, businesses rpi, business_categories as bc
WHERE
    rpi.name = 'Rensselaer Polytechnic Institute' and
    bc.business_id = b.business_id and
    (bc.category = 'Restaurants' or bc.category = 'Food')
ORDER BY dist
LIMIT 10;


SELECT 'Question 9';

(SELECT users.name, users.url
FROM users, reviews, businesses
WHERE
    reviews.user_id = users.user_id and
    reviews.business_id = businesses.business_id and
    businesses.city = 'Albany'
GROUP BY users.user_id
HAVING count(*) >= 2)
INTERSECT
(SELECT users.name, users.url
FROM users, reviews, businesses
WHERE
    reviews.user_id = users.user_id and
    reviews.business_id = businesses.business_id and
    businesses.city = 'Troy'
GROUP BY users.user_id
HAVING count(*) >= 2)
