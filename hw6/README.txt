Group Members: Noah Goldman

Run the program in the exact same way as the example python program from the homework prompt.

```
python hw6.py <input_file_name>
```

The example output for the test cases are included, and no known problems have been found.
