import os
import sys
import csv
import logging
from collections import OrderedDict

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

SCRIPT_DIR, _ = os.path.split(os.path.realpath(__file__))

TUPLE_SEPARATOR = ','

# Folders for the different tables/indices
MOVIEROLES_IDX_DIR = os.path.join(SCRIPT_DIR, 'movieroles_ma_idx')
MOVIEROLES_TABLE_DIR = os.path.join(SCRIPT_DIR, 'movieroles_table')
ACTORS_IDX_DIR = os.path.join(SCRIPT_DIR, 'actors_id_idx')
ACTORS_TABLE_DIR = os.path.join(SCRIPT_DIR, 'actors_table')

class CostsCounter:
    def __init__(self):
        self.reset()

    def increment(self, data_dir):
        if not data_dir in self.costs:
            self.costs[data_dir] = 1
        else:
            self.costs[data_dir] += 1

    def reset(self):
        self.costs = OrderedDict()

    def total(self):
        sum = 0
        for _, v in self.costs.items():
            sum += v
        return sum

    def print_summary(self, method_count):
        print('Method {0} total cost: {1} pages'.format(method_count, self.total()))
        for k, v in self.costs.items():
            _, data_name = os.path.split(k)
            print((' ' * 4) + '{0} page {1}{2}'.format(v, data_name,
                                              ' index' if 'idx' in data_name else ''))

def try_conv_int(s):
    try:
        return int(s)
    except ValueError:
        return s

def list_eq(l1, l2):
    return set(l1) == set(l2)

def getPageFilePath(dir, i):
    return os.path.join(dir, 'page{0}.txt'.format(i))

# Read a page file into a list of tuples
def readPage(fpath):
    tuples = []
    lines = []
    next_page = None
    page_type = None

    with open(fpath, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            lines.append(row)

    if len(lines[0]) > 1:
        tuples.append([try_conv_int(x) for x in lines[0]])
    else:
        page_type = lines[0][0]

    for line in lines[1:-1]:
        tuples.append([try_conv_int(x) for x in line])

    if len(lines[-1]) > 1:
        tuples.append([try_conv_int(x) for x in lines[-1]])
    elif len(lines[-1]) == 1:
        next_page = lines[-1][0]

    return tuples, page_type, next_page

# Given a list of tuples, return all for which the comparison function
# evaluates to true
def filterTuples(tuples, comparison_func):
    res = []
    for i in range(len(tuples)):
        if comparison_func(tuples[i], tuples[i+1] if (i+1) < len(tuples) else None):
            res.append(tuples[i])

    return res

def tuple_in_range(x, limits, cols):
    for c in range(cols+1):
        if x[c] < limits[c][0] or x[c] > limits[c][1]:
            return False
    return True

def search_index(index_dir, limits, counter):
    toSearch = [os.path.join(index_dir, 'root.txt')]
    followNext = False
    output_tuples = []
    while len(toSearch) > 0:
        logger.debug("Searching " + toSearch[0])

        counter.increment(index_dir)

        # open the index
        tuples, page_type, next_page = readPage(toSearch.pop(0))

        c = 0
        if len(limits) > 1 and page_type == 'leaf':
            c = 1

        if page_type == 'leaf':
            for x in tuples:
                valid = True
                for col in range(len(limits)):
                    if x[col] < limits[col][0] or x[col] > limits[col][1]:
                        valid = False
                if valid:
                    output_tuples.append(x)

            if followNext and next_page is not None:
                logger.debug('adding next page')
                new_path = os.path.join(index_dir, next_page)
                if new_path not in toSearch:
                    toSearch.insert(0, os.path.join(index_dir, next_page))
        else:
            # Iterate until the range starts
            start = None
            i = 0
            while start == None and i < len(tuples):
                valid = True
                for x in range(c+1):
                    if tuples[i][x] < limits[x][0]:
                        valid = False
                if valid:
                    start = i
                else:
                    i += 1

            if start is None:
                continue

            # Iterate until the range ends
            while i < len(tuples):
                valid = True
                for x in range(c+1):
                    if tuples[i][x] > limits[x][1]:
                        valid = False
                if not valid:
                    break
                i += 1

            # If we reached the end of the internal node without exiting the
            # range, just rely on next_page pointers instead of reading all
            # internal nodes
            if start == 0 and i == len(tuples):
                followNext = True
                toSearch.append(os.path.join(index_dir, tuples[0][len(tuples[0])-1]))
            else:
                toSearch += [os.path.join(index_dir, x[len(x)-1]) for x in tuples[start:i+1]]

    return output_tuples

def lookup_table(table_dir, id, id_col, page_id, counter):
    counter.increment(table_dir)

    page = getPageFilePath(table_dir, page_id)

    logger.debug('Looking up id={0} in {1}'.format(id, page))
    tuples, _, _ = readPage(page)

    res = [x for x in tuples if x[id_col] == id]

    # If there are multiple results, there is a problem
    assert len(res) < 2

    return res[0] if len(res) > 0 else None

def scan_table(table_dir, ids, id_col, counter):
    ids_left = list(ids)
    res = []

    i = 1
    pageFile = getPageFilePath(table_dir, i)
    while len(ids_left) > 0 and os.path.exists(pageFile):
        logger.debug('Looking for id={0} in {1}'.format(id, pageFile))

        counter.increment(table_dir)
        tuples, _, _ = readPage(pageFile)

        found = [x for x in tuples if x[id_col] in ids_left]

        for x in found:
            assert x[id_col] in ids_left
            ids_left.remove(x[id_col])

        res += found

        i += 1
        pageFile = getPageFilePath(table_dir, i)

    return res

def scan_table_func(table_dir, func, counter):
    res = []

    i = 1
    pageFile = getPageFilePath(table_dir, i)
    while os.path.exists(pageFile):
        logger.debug('Scanning by a function in {1}'.format(id, pageFile))

        counter.increment(table_dir)
        tuples, _, _ = readPage(pageFile)

        found = [x for x in tuples if func(x)]
        res += found

        i += 1
        pageFile = getPageFilePath(table_dir, i)

    return res


# Scan movieroles_ma_idx to find all actors, then actors_id_idx to find the
# names of the actors
def method1(limits):
    counter = CostsCounter()

    ma_res = search_index(MOVIEROLES_IDX_DIR, limits, counter)

    actors = []

    # map the results to just actor ids
    actor_ids = [x[1] for x in ma_res]
    actor_ids = set(actor_ids)
    for id in actor_ids:
        actors += search_index(ACTORS_IDX_DIR, ((id, id),), counter)

    res = []
    for actor in actors:
        res.append(lookup_table(ACTORS_TABLE_DIR, actor[0], 1, actor[1], counter))

    return [x[2] + ' ' + x[3] for x in res], counter

# Scan movieroles_ma_idx to find all actors, then scan actors_table for the names
def method2(limits):
    counter = CostsCounter()

    ma_res = search_index(MOVIEROLES_IDX_DIR, limits, counter)

    actor_ids = [x[1] for x in ma_res]
    actor_ids = set(actor_ids)
    actors = scan_table(ACTORS_TABLE_DIR, actor_ids, 1, counter)

    return [x[2] + ' ' + x[3] for x in actors], counter

# Scan movieroles_table to find all corresponding entries, then scan
# actors_table for the names
def method3(limits):
    counter = CostsCounter()

    def scan_func(x):
        return (x[0] >= limits[1][0] and x[0] <= limits[1][1]
                and x[3] >= limits[0][0] and x[3] <= limits[0][1])

    mt_res = scan_table_func(MOVIEROLES_TABLE_DIR, scan_func, counter)

    # get just actor ids
    actor_ids = set([x[0] for x in mt_res])

    actors = scan_table(ACTORS_TABLE_DIR, actor_ids, 1, counter)

    return [x[2] + ' ' + x[3] for x in actors], counter

def print_results(results):
    print('\nResults ({0} total):'.format(len(results)))
    results.sort()
    for x in results:
        print((' ' * 4) + x)
    print('')

def run_methods(input_str):
    limits_lst = [try_conv_int(x) for x in input_str.split(',')]
    limits = [(limits_lst[0], limits_lst[1]), (limits_lst[2], limits_lst[3])]

    for i in range(len(limits)):
        lower = limits[i][0]
        upper = limits[i][1]
        if limits[i][0] == '*':
            lower = -1
        if limits[i][1] == '*':
            upper = sys.maxsize
        limits[i] = (lower, upper)

    counters = [None, None, None]

    res1, counters[0] = method1(limits)
    res2, counters[1] = method2(limits)
    res3, counters[2] = method3(limits)

    assert list_eq(res1, res2)
    assert list_eq(res2, res3)

    print('Query: ' + input_str)

    print_results(res1)

    for i in range(len(counters)):
        if counters[i] is not None:
            counters[i].print_summary(i+1)
            print('')

    print('**************************************')

if __name__ == '__main__':
    _, input_file = sys.argv
    with open(input_file, 'r') as f:
        for line in f.read().split('\n'):
            if line:
                run_methods(line)
